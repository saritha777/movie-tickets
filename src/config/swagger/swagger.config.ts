import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  /**
   * title property
   */
  title: 'Movie tickets booking application',
  /**
   * description property
   */
  description: 'movie tickets booking application',
  /**
   * version property
   */
  version: '1.0',
  /**
   * tags property
   */
  tags: ['templete'],
};
