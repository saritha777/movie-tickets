/**
 * swagger config interface
 */
export interface SwaggerConfig {
  /**
   * title property
   */
  title: string;
  /**
   * description property
   */
  description: string;
  /**
   * version property
   */
  version: string;
  /**
   * tags property
   */
  tags: string[];
}
