import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Payment } from 'src/entity/pyment.entity';
import { PaymentService } from './payment.service';

/**
 * payment controller
 */
@ApiTags('Payment')
@Controller('Payment')
export class PaymentController {
  /**
   * payment service having all business logics
   * @param paymentService injecting payment service
   */
  constructor(private readonly paymentService: PaymentService) {}

  /**
   * add method for payment
   * @param data taking payment details
   * @returns payment data
   */
  @Post()
  async addPayment(@Body() data: Payment): Promise<Payment> {
    return await this.paymentService
      .addPayment(data)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException(
            'payment data not available',
            HttpStatus.NOT_FOUND,
          );
        }
      })

      .catch(() => {
        throw new HttpException(
          'payment data not available',
          HttpStatus.NOT_FOUND,
        );
      });
  }
}
