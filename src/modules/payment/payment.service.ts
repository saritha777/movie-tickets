import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpExceptionFilter } from 'src/common/filters/http-exception.filter';
import { UserLogin } from 'src/entity/login.entity';
import { Movies } from 'src/entity/movies.entity';
import { Payment } from 'src/entity/pyment.entity';
import { Theatres } from 'src/entity/theatres.entity';
import { Repository } from 'typeorm';

/**
 * payment service habving all business logics
 */
@Injectable()
export class PaymentService {
  /**
   * injecting all repository
   * @param paymentRepository injecting payment repository
   * @param loginRepository injecting login repository
   * @param moviesRepository injecting movie repository
   * @param theatreRepository injecting theatre repository
   */
  constructor(
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    @InjectRepository(Movies)
    private moviesRepository: Repository<Movies>,
    @InjectRepository(Theatres)
    private theatreRepository: Repository<Theatres>,
  ) {}

  /**
   * add method for payment
   * @param data passing payment details
   * @returns payment details
   */
  async addPayment(data: Payment) {
    const paymentData = new Payment();
    const loginData: UserLogin = await this.loginRepository.findOne({
      id: data.userId,
    });
    if (!loginData) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    } else {
      const movieData: Movies = await this.moviesRepository.findOne({
        id: data.movieId,
      });
      if (!movieData) {
        throw new HttpException(
          'movie data not available',
          HttpStatus.NOT_FOUND,
        );
      } else {
        const theatreData: Theatres = await this.theatreRepository.findOne({
          id: data.theatreId,
        });
        if (!theatreData) {
          throw new HttpException(
            'theatre not available',
            HttpStatus.NOT_FOUND,
          );
        } else {
          paymentData.login = loginData;
          paymentData.userId = loginData.id;
          paymentData.movieId = movieData.id;
          paymentData.movieName = movieData.movieName;
          paymentData.theatreId = theatreData.id;
          paymentData.theatreName = theatreData.theatreName;
          paymentData.cardHolderName = data.cardHolderName;
          paymentData.cardNo = data.cardNo;
          paymentData.cvv = data.cvv;
          paymentData.expiry = data.expiry;
          paymentData.noofTickets = data.noofTickets;
          paymentData.price = data.price = movieData.price;
          paymentData.totalPrice = paymentData.price * paymentData.noofTickets;

          return await this.paymentRepository.save(paymentData);
        }
      }
    }
  }
}
