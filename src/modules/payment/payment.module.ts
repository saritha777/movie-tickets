import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLogin } from 'src/entity/login.entity';
import { Movies } from 'src/entity/movies.entity';
import { Payment } from 'src/entity/pyment.entity';
import { Theatres } from 'src/entity/theatres.entity';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';

@Module({
  imports: [TypeOrmModule.forFeature([Payment, UserLogin, Movies, Theatres])],
  controllers: [PaymentController],
  providers: [PaymentService],
})
export class PaymentModule {}
