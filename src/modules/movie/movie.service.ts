import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movies } from 'src/entity/movies.entity';
import { Repository } from 'typeorm';

/**
 * movie service having all business logics
 */
@Injectable()
export class MoviesService {
  /**
   * injecting movie repository
   * @param movieRepository injecting movie repository
   */
  constructor(
    @InjectRepository(Movies)
    private movieRepository: Repository<Movies>,
  ) {}

  /**
   * add method for adding movie details
   * @param data taking movie data
   * @returns saving movie details
   */
  async addMovie(data: Movies): Promise<Movies> {
    const movieData = new Movies();
    const newData = Object.assign(movieData, data);
    console.log(newData);
    return this.movieRepository.save(newData);
  }

  /**
   * getting all movie details
   * @returns getting all movie details
   */
  async getAllMovies(): Promise<Movies[]> {
    return this.movieRepository.find();
  }

  /**
   * get method based on movie name
   * @param name taking name for movie details
   * @returns getting all movie details based on name
   */
  async getMoviesByName(name: string): Promise<Movies> {
    return this.movieRepository.findOne({ movieName: name });
  }

  /**
   * update method for movie data
   * @param id taking id to update
   * @param data taking movie details for update
   * @returns updated information
   */
  async updateMovieData(id: number, data: Movies) {
    const movieData = new Movies();
    const newData = Object.assign(movieData, data);
    await this.movieRepository.update({ id: id }, newData);
    return 'theatre data updates successfully';
  }

  /**
   * delete method based on name
   * @param name taking name to delete
   * @returns delete information
   */
  async deleteMovieByName(name: string) {
    return this.movieRepository.delete({ movieName: name });
  }
}
