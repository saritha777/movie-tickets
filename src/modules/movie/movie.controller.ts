import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Movies } from 'src/entity/movies.entity';
import { MoviesService } from './movie.service';

/**
 * movie controller front end for storing data
 */
@ApiTags('MOVIES')
@Controller('/Movies')
export class MoviesController {
  /**
   * injecting movie service for business logic
   * @param moviesService injecting movie service
   */
  constructor(private readonly moviesService: MoviesService) {}

  /**
   * post method for movie data
   * @param movie taking movie details
   * @returns saving movie data
   */
  @Post()
  async addMovie(@Body() movie: Movies): Promise<Movies> {
    return this.moviesService.addMovie(movie);
  }

  /**
   * getting all movie details
   * @returns getting all details
   */
  @Get()
  async getAllMovieData(): Promise<Movies[]> {
    return await this.moviesService
      .getAllMovies()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException(
            'movie details not available',
            HttpStatus.NOT_FOUND,
          );
        }
      })

      .catch(() => {
        throw new HttpException(
          'movie details not available',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  /**
   * get method based on movie name
   * @param movieName taking movie name
   * @returns movie details based on name
   */
  @Get('/:movieName')
  async getMovieDataByName(
    @Param('movieName') movieName: string,
  ): Promise<Movies> {
    return await this.moviesService
      .getMoviesByName(movieName)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException(
            'movie details not available',
            HttpStatus.NOT_FOUND,
          );
        }
      })

      .catch(() => {
        throw new HttpException(
          'movie details not available',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  /**
   * update method for movie data
   * @param id taking to update
   * @param data taking data to update
   * @returns updated information
   */
  @Put('/:id')
  async updateMovieData(@Param('id') id: number, @Body() data: Movies) {
    return await this.moviesService.updateMovieData(id, data);
  }

  /**
   * delete method for movies
   * @param movieName taking movie name
   * @returns deleted information
   */
  @Delete('/:movieName')
  async deleteTheatre(@Param('movieName') movieName: string) {
    return await this.moviesService.deleteMovieByName(movieName);
  }
}
