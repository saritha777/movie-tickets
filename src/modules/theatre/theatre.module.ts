import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Movies } from 'src/entity/movies.entity';
import { Theatres } from 'src/entity/theatres.entity';
import { TheatreController } from './theatre.controller';
import { TheatreService } from './theatre.service';

@Module({
  imports: [TypeOrmModule.forFeature([Theatres, Movies])],
  controllers: [TheatreController],
  providers: [TheatreService],
})
export class TheatreModule {}
