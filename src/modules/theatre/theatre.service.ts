import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movies } from 'src/entity/movies.entity';
import { Theatres } from 'src/entity/theatres.entity';
import { Repository } from 'typeorm';

/**
 * theatre service having all business logics
 */
@Injectable()
export class TheatreService {
  /**
   * injecting theatre repository for saving details to data base
   * @param theatreRepository injecting theatre repository
   */
  constructor(
    @InjectRepository(Theatres)
    private theatreRepository: Repository<Theatres>,
    @InjectRepository(Movies)
    private movieRepository: Repository<Movies>,
  ) {}

  /**
   * add method for the theatre details
   * @param id taking movie id for movie details
   * @param data taking theatre data
   * @returns saving thetre data to data dase
   */
  async addTheatre(id: number, data: Theatres): Promise<Theatres> {
    const theatreData = new Theatres();
    const movieData = await this.movieRepository.findOne({ id: id });
    theatreData.movie = movieData;
    const newData = Object.assign(theatreData, data);
    console.log(newData);
    return this.theatreRepository.save(newData);
  }

  /**
   * get all methods for theatre
   * @returns all theatre dtails
   */
  async getAllTheatres(): Promise<Theatres[]> {
    return this.theatreRepository.find();
  }

  /**
   * get method for theatre name
   * @param name passing theatre name
   * @returns getting details based on thetre name
   */
  async getTheatreByName(name: string) {
    return await this.theatreRepository.findOne({ theatreName: name });
  }

  /**
   * update method for theatre data
   * @param id taking id which we need to update
   * @param movieId taking movie id for movie details
   * @param data taking updated data
   * @returns saving updated information
   */
  async updateTheatreData(id: number, movieId: number, data: Theatres) {
    const theatreData = new Theatres();
    const movieData = await this.movieRepository.findOne({ id: movieId });
    theatreData.movie = movieData;
    const newData = Object.assign(theatreData, data);
    await this.theatreRepository.update({ id: id }, newData);
    return 'theatre data updates successfully';
  }

  /**
   * dalete method for theatre name
   * @param name taking username to delete
   * @returns deleted info
   */
  async deleteTheatreByName(name: string) {
    return this.theatreRepository.delete({ theatreName: name });
  }
}
