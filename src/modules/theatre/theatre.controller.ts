import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Theatres } from 'src/entity/theatres.entity';
import { TheatreService } from './theatre.service';

/**
 * theatre controller for connecting to front end
 */
@ApiTags('THEATRES')
@Controller('/theatres')
export class TheatreController {
  /**
   * theatre service having all the business logics
   * @param theatreService injecting theatre service
   */
  constructor(private readonly theatreService: TheatreService) {}

  /**
   * method for adding theatre details
   * @param movieId taking movie id for movie details
   * @param theatre passing theatre data
   * @returns saving thetre data to data base
   */
  @Post('/:movieId')
  async addTheatre(
    @Param('movieId') movieId: number,
    @Body() theatre: Theatres,
  ): Promise<Theatres> {
    return await this.theatreService.addTheatre(movieId, theatre);
  }

  /**
   * get method for all theatres
   * @returns get all theatres
   */
  @Get()
  async getAllTheatreData(): Promise<Theatres[]> {
    return await this.theatreService
      .getAllTheatres()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException(
            'theatre details not available',
            HttpStatus.NOT_FOUND,
          );
        }
      })

      .catch(() => {
        throw new HttpException(
          'theatre details not available',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  /**
   * get method based on theatre name
   * @param theatreName passing name for taking theatre details
   * @returns
   */
  @Get('/name/:theatreName')
  async getTheatresbyName(
    @Param('theatreName') theatreName: string,
  ): Promise<Theatres> {
    return await this.theatreService
      .getTheatreByName(theatreName)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException(
            'theatre details not available',
            HttpStatus.NOT_FOUND,
          );
        }
      })

      .catch(() => {
        throw new HttpException(
          'theatre details not available',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  /**
   * update method for theatre details
   * @param id taking id to update
   * @param movieId taking movie id for movie details
   * @param data taking theatre data for update
   * @returns updated information
   */
  @Put('/:id/:movieId')
  async updateTheatreData(
    @Param('id') id: number,
    @Param('movieId') movieId: number,
    @Body() data: Theatres,
  ) {
    return await this.theatreService.updateTheatreData(id, movieId, data);
  }

  /**
   * delete method based on theatre name
   * @param theatreName taking theatreName to delete
   * @returns deleted information
   */
  @Delete('/:theatreName')
  async deleteTheatre(@Param('theatreName') theatreName: string) {
    return await this.theatreService.deleteTheatreByName(theatreName);
  }
}
