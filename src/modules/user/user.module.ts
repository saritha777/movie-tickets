import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtToken } from 'src/common/providers/jwtservice';
import { UserLogin } from 'src/entity/login.entity';
import { Movies } from 'src/entity/movies.entity';
import { Payment } from 'src/entity/pyment.entity';
import { User } from 'src/entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLogin]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' },
    }),
  ],
  controllers: [UserController],
  providers: [UserService, JwtToken],
})
export class UserModule {}
