import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../entity/user.entity';
import { Payment } from './pyment.entity';

/**
 * user login entity
 */
@Entity()
export class UserLogin {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @OneToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  user: User;

  @OneToOne(() => Payment, (payment) => payment.login, {
    cascade: true,
  })
  @JoinColumn()
  payment: Payment;
}
