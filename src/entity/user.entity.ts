import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsInt,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';
import { Maintainance } from './maintainance';

/**
 * user entity
 */
@Entity()
export class User extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column({ unique: true })
  firstName: string;

  @ApiProperty()
  @IsString()
  @Column()
  lastName: string;

  @ApiProperty()
  @IsEmail()
  @Column({ unique: true })
  email: string;

  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  @Column({ unique: true })
  password: string;

  @ApiProperty()
  @IsInt()
  @Column()
  phoneNo: number;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true,
  })
  login: UserLogin;
}
