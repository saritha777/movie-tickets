import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString } from 'class-validator';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Maintainance } from './maintainance';
import { Theatres } from './theatres.entity';

/**
 * movie entity
 */
@Entity()
export class Movies extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  movieName: string;

  @ApiProperty()
  @IsString()
  @Column()
  review: string;

  @ApiProperty()
  @IsInt()
  @Column()
  rating: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  @Column()
  price: number;

  @ManyToMany(() => Theatres, (theatre) => theatre.movie)
  theatre: Theatres;
}
