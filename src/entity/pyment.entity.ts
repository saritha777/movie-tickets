import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';

/**
 * payment entity
 */
@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsInt()
  @Column()
  userId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  movieId: number;

  @Column()
  movieName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  theatreId: number;

  @Column()
  theatreName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  noofTickets: number;

  @Column()
  price: number;

  @Column()
  totalPrice: number;

  @ApiProperty()
  @IsInt()
  @Column()
  cardNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  cardHolderName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  cvv: number;

  @ApiProperty()
  @IsString()
  @Column()
  expiry: string;

  @OneToOne(() => UserLogin, (login) => login.payment)
  login: UserLogin;
}
