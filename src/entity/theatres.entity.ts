import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Maintainance } from './maintainance';
import { Movies } from './movies.entity';

/**
 * theatre entity
 */
@Entity()
export class Theatres extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  theatreName: string;

  @ApiProperty()
  @IsString()
  @Column()
  showTimings: string;

  @ApiProperty()
  @IsString()
  @Column()
  address: string;

  @ManyToMany(() => Movies, (movie) => movie.theatre, {
    cascade: true,
  })
  @JoinColumn()
  movie: Movies;
}
