'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">sample documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' : 'data-target="#xs-controllers-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' :
                                            'id="xs-controllers-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' : 'data-target="#xs-injectables-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' :
                                        'id="xs-injectables-links-module-AppModule-f5eb7b2c4c26d31e91bd9054d1b6b1b2f82422c69b958b0970264ead1c53150f27276308c2b2d29b42c3136b1f1ab12ebefad1333c539e71dbce63bb7ee48caa"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MoviesModule.html" data-type="entity-link" >MoviesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' : 'data-target="#xs-controllers-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' :
                                            'id="xs-controllers-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' }>
                                            <li class="link">
                                                <a href="controllers/MoviesController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MoviesController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' : 'data-target="#xs-injectables-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' :
                                        'id="xs-injectables-links-module-MoviesModule-4616260403a6f843eb198a8dd3fe9a6b00a34c195a27d06e9a26676ddeab5dbe49c4f80a954a42b3b948c4fb75363d34671e9138e88311bd2d0ad8d7b9ac7472"' }>
                                        <li class="link">
                                            <a href="injectables/MoviesService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MoviesService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentModule.html" data-type="entity-link" >PaymentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' : 'data-target="#xs-controllers-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' :
                                            'id="xs-controllers-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' }>
                                            <li class="link">
                                                <a href="controllers/PaymentController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' : 'data-target="#xs-injectables-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' :
                                        'id="xs-injectables-links-module-PaymentModule-c58a035dc8a1716c70e59e0507b23fb9728b132cc9e4690cb9bc51454c466b590cdbcd1c340a733b92f363e2c20767f0f233bf3cd63e8c279af8e3ead7f0be88"' }>
                                        <li class="link">
                                            <a href="injectables/PaymentService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TheatreModule.html" data-type="entity-link" >TheatreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' : 'data-target="#xs-controllers-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' :
                                            'id="xs-controllers-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' }>
                                            <li class="link">
                                                <a href="controllers/TheatreController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TheatreController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' : 'data-target="#xs-injectables-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' :
                                        'id="xs-injectables-links-module-TheatreModule-03b8e2bdb015b54192f5b0b14ce206956cf73ad92107c65a7404e9a729ab5d733d428ae4629f9a948129abd0f2e78045576a3c8d44813df797a6a5a6068d24b4"' }>
                                        <li class="link">
                                            <a href="injectables/TheatreService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TheatreService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' : 'data-target="#xs-controllers-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' :
                                            'id="xs-controllers-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' : 'data-target="#xs-injectables-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' :
                                        'id="xs-injectables-links-module-UserModule-aef254cc8b249b7cf49906f0bdeb0f47c3423975fa0523539285f950a94fe0a572b27c15498751a10d672dfff41533c25f6205aab107f65e4570297f6dfc3042"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/MoviesController.html" data-type="entity-link" >MoviesController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PaymentController.html" data-type="entity-link" >PaymentController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/TheatreController.html" data-type="entity-link" >TheatreController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Movies.html" data-type="entity-link" >Movies</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Payment.html" data-type="entity-link" >Payment</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Theatres.html" data-type="entity-link" >Theatres</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/Maintainance.html" data-type="entity-link" >Maintainance</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MoviesService.html" data-type="entity-link" >MoviesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PaymentService.html" data-type="entity-link" >PaymentService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TheatreService.html" data-type="entity-link" >TheatreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});